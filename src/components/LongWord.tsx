import React from 'react';
import lists from '../list.json';
import { couldBeShortcut } from '../keyboardEvent';

type MaskEntry = { cover: number, revealed: boolean };
type Mask = Array<MaskEntry>;

const topWordsList = (lists as { long: { top: string[] } }).long.top;

type MaskUpdateOptions = { solution: string, action: 'one' | 'all' | 'regenerate' };
const maskRevealer: React.Reducer<Mask, MaskUpdateOptions> = (prevMask, options) => {
  if (options.action === 'all') {
    return prevMask.map((_maskEntry, index) => ({ cover: index, revealed: true }));
  }

  if (options.action === 'one') {
    const unrevealedEntries = Array.from(prevMask.entries())
      .filter(([_index, maskEntry]) => !maskEntry.revealed);
    const [charToReveal, maskEntryToReveal] = pickRandom(unrevealedEntries);
    const [positionInMaskOfCharToReveal] = unrevealedEntries
      // We should be able to assume we found one here, hence the `!`.
      .find(([position, maskEntry]) => maskEntry.cover === charToReveal)!;

    const newMask = [...prevMask];
    newMask[positionInMaskOfCharToReveal] = { cover: maskEntryToReveal.cover, revealed: false };
    newMask[charToReveal] = { cover: charToReveal, revealed: true };
    return newMask;
  }

  return generateMask(options.solution);
}

const generateSolution = () => {
  return pickRandom(topWordsList);
}
const firstSolution = generateSolution();
const firstMask = generateMask(firstSolution);

interface Props {
  timeout?: number;
};

export const LongWord: React.FC<Props> = ({
  timeout = -1,
} = {}) => {
  const wrapperRef = React.useRef<HTMLDivElement>(null);
  const [solution, setSolution] = React.useState(firstSolution);
  const [mask, updateMask] = React.useReducer(maskRevealer, firstMask);
  React.useMemo(() => updateMask({solution, action: 'regenerate'}), [solution]);
  React.useLayoutEffect(() => {
    if (solution && wrapperRef.current) {
      wrapperRef.current.focus();
    }
  }, [solution, wrapperRef]);
  React.useEffect(() => {
    if (timeout === -1 || !solution || !mask || mask.every(maskEntry => maskEntry.revealed)) {
      return;
    }

    const timeoutRef = setTimeout(() => {
      updateMask({ solution, action: 'one' });
    }, timeout * 1000);
    return () => clearTimeout(timeoutRef);
  }, [mask, solution, timeout]);

  const handleKeyPress: React.KeyboardEventHandler = (event) => {
    // Do not override browser shortcuts:
    if (couldBeShortcut(event)) {
      return;
    }

    if (mask.every((maskEntry) => maskEntry.revealed)) {
      event.preventDefault();
      return setSolution(generateSolution());
    }

    if (' ' === event.key) {
      event.preventDefault();
      return updateMask({ solution, action: 'one' });
    }
    if (event.key === 'Enter') {
      event.preventDefault();
      return updateMask({ solution, action: 'all' });
    }
  };
  const handleTouch: React.TouchEventHandler = (event) => {
    event.preventDefault();

    if (mask.every((maskEntry) => maskEntry.revealed)) {
      return setSolution(generateSolution());
    }

    return updateMask({ solution, action: 'one' });
  }

  const cards = mask.map((maskEntry) => {
    const validationClass = (maskEntry.revealed) ? 'correct' : 'close';
    const char = solution.charAt(maskEntry.cover);

    return (
      <div key={'char' + maskEntry.cover} className={`level-item is-size-1 charGuess ${validationClass}`}>
        <span className="letter">
          {char}
        </span>
      </div>
    );
  });

  return <>
    <div
      tabIndex={0}
      onKeyDown={handleKeyPress}
      onTouchEnd={handleTouch}
      ref={wrapperRef}
      className="game longMode"
    >
      <div className="level is-mobile guess">
        {cards}
      </div>
    </div>
  </>;
};

function generateMask (word: string): Mask {
  const coverIndices = word.split('').map((_, i) => i);

  for (let i = 0; i < coverIndices.length; i += 1) {
    let rand: number = Math.floor(i + Math.random() * (coverIndices.length - i));
    [coverIndices[rand], coverIndices[i]] = [coverIndices[i], coverIndices[rand]]
  }

  return coverIndices.map((index) => ({ cover: index, revealed: false }));
}

function pickRandom<T>(list: T[]): T {
  return list[Math.floor(Math.random() * list.length)];
}
